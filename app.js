const cardGenerate = document.querySelector(".card");
const btnGenerate = document.querySelector(".btn-generate");

function info() {
  const data = [
    {
      title : 'card_1',
      src : './src/Images/image1.webp'      
    },
    {
      title : 'card_2',
      src : './src/Images/Image2.jpg'      
    },
    {
      title : 'card_3',
      src : './src/Images/Image3.jpeg'      
    },
    {
      title : 'card_4',
      src : './src/Images/Image4.jpeg'      
    },
    {
      title : 'card_5',
      src : './src/Images/Image5.jpg'      
    },
    {
      title : 'card_6',
      src : './src/Images/Image6.jpg'      
    },
    {
      title : 'card_7',
      src : './src/Images/Image7.jpeg'      
    },
    {
      title : 'card_8',
      src : './src/Images/Image8.webp'      
    },
    {
      title : 'card_9',
      src : './src/Images/Image9.jpg'      
    },
    {
      title : 'card_10',
      src : './src/Images/Image10.jpg'      
    },
    {
      title : 'card_11',
      src : './src/Images/Image11.png'      
    },
    {
      title : 'card_12',
      src : './src/Images/Image12.jpg'      
    },
  ];
  return data;  
}

const eventListener = () => {
  btnGenerate.addEventListener('click', generate_Card);
}

eventListener();

function generate_Card() {

  const cardName = document.querySelector(".name");
  const cardLocation = document.querySelector(".location");
  const cardOrigin = document.querySelector(".origin");
  const figure = document.querySelector('.main-image');
  const currentCard = generateCharacter();
    setTimeout(()=>{
    figure.removeAttribute('src');
    figure.setAttribute('src', currentCard[0]);     
    cardName.textContent = 'Name: ' + currentCard[1];     
    cardLocation.textContent = 'Location: ' + currentCard[2];     
    cardOrigin.textContent = 'Origin: ' + currentCard[3];       
    },100);          
} 

function generateCharacter() {

  const randomCharacter = Math.floor((Math.random() * (20-1))+0);
  const characters = [];

  const request = fetch('https://rickandmortyapi.com/api/character');

    request.then(response => response.json())
    .then(result => {
        characters[0] = result.results[randomCharacter].image;                
        characters[1] = result.results[randomCharacter].name;        
        characters[2] = result.results[randomCharacter].location.name;        
        characters[3] = result.results[randomCharacter].origin.name;        
      });    
    return characters;
  }
